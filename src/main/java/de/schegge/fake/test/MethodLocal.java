package de.schegge.fake.test;

import java.util.Objects;
import java.util.function.Supplier;

public class MethodLocal<T> {

  private final Supplier<? extends T> supplier;
  private T value;

  public MethodLocal(Supplier<? extends T> supplier) {
    this.supplier = Objects.requireNonNull(supplier);
  }

  public T get() {
    if (value == null) {
      value = supplier.get();
    }
    return value;
  }
}
