package de.schegge.fake.test;

import de.schegge.fake.persona.PersonaAttribute;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
public @interface Alias {

  PersonaAttribute attribute();

  String field();
}
