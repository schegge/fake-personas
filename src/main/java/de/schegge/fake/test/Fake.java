package de.schegge.fake.test;

import de.schegge.fake.persona.PersonaAttribute;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.PARAMETER, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface Fake {

  PersonaAttribute[] ignore() default {};

  String persona() default "";

  String value() default "";

  String json() default "";

  Alias[] alias() default {};
}
