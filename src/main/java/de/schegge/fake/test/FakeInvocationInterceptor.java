package de.schegge.fake.test;

import static de.schegge.fake.persona.feature.Feature.EMAIL_BY_USERNAME;
import static de.schegge.fake.persona.feature.Feature.NULL_ON_MISSING_ATTRIBUTE;
import static de.schegge.fake.persona.feature.Feature.OVERRIDE_ZERO_VALUES;
import static java.util.Objects.requireNonNullElse;
import static java.util.stream.Collectors.toMap;

import de.schegge.fake.persona.FakePersonaBuilder;
import de.schegge.fake.persona.PersonaAttribute;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.Map;
import java.util.stream.Stream;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.InvocationInterceptor;
import org.junit.jupiter.api.extension.ReflectiveInvocationContext;

public class FakeInvocationInterceptor implements InvocationInterceptor {

  @Override
  public void interceptTestMethod(Invocation<Void> invocation, ReflectiveInvocationContext<Method> invocationContext,
      ExtensionContext extensionContext) throws Throwable {
    Parameter[] parameters = invocationContext.getExecutable().getParameters();
    MethodLocal<FakePersonaBuilder> builder = new MethodLocal<>(
        () -> FakePersonaBuilder.create().enable(EMAIL_BY_USERNAME, OVERRIDE_ZERO_VALUES, NULL_ON_MISSING_ATTRIBUTE)
    );
    for (int i = 0, n = parameters.length; i < n; i++) {
      Fake annotation = parameters[i].getAnnotation(Fake.class);
      if (annotation != null) {
        Map<String, PersonaAttribute> aliases = Stream.of(annotation.alias())
            .collect(toMap(Alias::field, Alias::attribute));
        builder.get().copy().with(annotation.json()).alias(aliases)
            .ignore(annotation.ignore())
            .build(requireNonNullElse(annotation.persona(), annotation.value()),
                invocationContext.getArguments().get(i));
      }
    }
    invocation.proceed();
  }
}
