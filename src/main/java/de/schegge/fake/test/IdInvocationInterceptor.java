package de.schegge.fake.test;

import static java.util.function.Function.identity;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.Map;
import java.util.function.Function;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.InvocationInterceptor;
import org.junit.jupiter.api.extension.ReflectiveInvocationContext;

public class IdInvocationInterceptor implements InvocationInterceptor {
  private static final Map<Class<?>, Function<Long, ?>> functions = Map.ofEntries(
      Map.entry(Integer.class, Long::intValue), Map.entry(int.class, Long::intValue),
      Map.entry(Long.class, identity()), Map.entry(long.class, identity())
  );

  @Override
  public void interceptTestMethod(Invocation<Void> invocation, ReflectiveInvocationContext<Method> invocationContext,
      ExtensionContext extensionContext) throws Throwable {
    Parameter[] parameters = invocationContext.getExecutable().getParameters();
    for (int i = 0, n = parameters.length; i < n; i++) {
      Id id = parameters[i].getAnnotation(Id.class);
      Object argument = invocationContext.getArguments().get(i);
      Field field = argument.getClass().getDeclaredField("id");
      Object value = calculateId(field, id == null ? 1L : id.value());
      if (value != null) {
        field.setAccessible(true);
        field.set(argument, value);
      }
    }
    invocation.proceed();
  }

  private Object calculateId(Field id, long value) {
    return functions.get(id.getType()).apply(value);
  }
}
