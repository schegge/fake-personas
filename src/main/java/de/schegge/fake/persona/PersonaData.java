package de.schegge.fake.persona;

import static java.util.Collections.emptyList;

import java.security.SecureRandom;
import java.util.List;
import java.util.Map;

public class PersonaData {

  private final Map<PersonaAttribute, List<String>> data;
  private final boolean nullOnMissingAttribute;
  private final boolean randomValues;
  private final SecureRandom secureRandom;

  public PersonaData(Map<PersonaAttribute, List<String>> data, boolean nullOnMissingAttribute, boolean randomValues,
      SecureRandom secureRandom) {
    this.data = data;
    this.nullOnMissingAttribute = nullOnMissingAttribute;
    this.randomValues = randomValues;
    this.secureRandom = secureRandom;
  }

  public String get(PersonaAttribute personaAttribute) {
    List<String> value = data.getOrDefault(personaAttribute, emptyList());
    if (value.isEmpty()) {
      if (!nullOnMissingAttribute) {
        throw new ConversionException("missing value for attribute " + personaAttribute);
      }
      return null;
    }
    return value.get(randomValues ? secureRandom.nextInt(value.size()) : 0);
  }
}
