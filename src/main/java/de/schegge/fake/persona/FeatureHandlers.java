package de.schegge.fake.persona;

import static de.schegge.fake.persona.feature.Feature.EMAIL_BY_USERNAME;
import static de.schegge.fake.persona.feature.Feature.LOGIN_BY_USERNAME;
import static java.util.stream.Collectors.toList;

import de.schegge.fake.persona.feature.EmailByUsernameFeatureHandler;
import de.schegge.fake.persona.feature.Feature;
import de.schegge.fake.persona.feature.FeatureHandler;
import de.schegge.fake.persona.feature.LoginByUsernameFeatureHandler;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Supplier;

public class FeatureHandlers {

  private static final Map<Feature, Supplier<FeatureHandler>> factories = Map.ofEntries(
      Map.entry(EMAIL_BY_USERNAME, EmailByUsernameFeatureHandler::new),
      Map.entry(LOGIN_BY_USERNAME, LoginByUsernameFeatureHandler::new)
  );

  private final List<FeatureHandler> featureHandlerList;
  private final List<PersonaAttribute> handledAttributes;

  public FeatureHandlers(Set<Feature> features, Set<PersonaAttribute> personaAttributes) {
    featureHandlerList = features.stream().map(factories::get)
        .filter(Objects::nonNull).map(Supplier::get)
        .filter(f -> personaAttributes.contains(f.getHandledAttribute())).collect(toList());

    handledAttributes = featureHandlerList.stream().map(FeatureHandler::getHandledAttribute)
        .collect(toList());
  }

  public boolean isHandled(PersonaAttribute attribute) {
    return handledAttributes.contains(attribute);
  }

  public void handle(Map<PersonaAttribute, List<String>> input, MapGate output,
      Map<PersonaAttribute, String> currentValues) {
    featureHandlerList.forEach(featureHandler -> featureHandler.handle(input, output, currentValues));
  }
}
