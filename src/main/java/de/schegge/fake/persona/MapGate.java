package de.schegge.fake.persona;

import de.schegge.fake.persona.feature.Feature;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class MapGate {

  private final boolean overrideZeroValues;
  private final boolean overrideExistingAttribute;
  private final List<String> allowedKeys;
  private final Map<String, Object> map;

  public MapGate(Map<String, Object> map, Set<Feature> features, List<String> allowedKeys) {
    this.map = map;
    overrideZeroValues = features.contains(Feature.OVERRIDE_ZERO_VALUES);
    overrideExistingAttribute = features.contains(Feature.OVERRIDE_EXITING_VALUES);
    this.allowedKeys = allowedKeys;
  }

  public void putIfAllowed(String key, Object value) {
    if (overrideExistingAttribute) {
      map.put(key, value);
      return;
    }
    if (allowedKeys.contains(key)) {
      map.put(key, value);
    }
    Object v = map.get(key);
    if (v == null || v.equals(0) && overrideZeroValues) {
      map.put(key, value);
    }
  }

  public Map<String, Object> getMap() {
    return map;
  }
}
