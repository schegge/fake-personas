package de.schegge.fake.persona;

import com.fasterxml.jackson.annotation.JsonValue;

public enum PersonaAttribute {
  NONE,
  GENDER,
  SALUTATION,
  TITLE,
  FIRSTNAME,
  LASTNAME,
  LOGIN,
  EMAIL,
  PHONE,
  STREET,
  HOUSENUMBER,
  ZIP,
  CITY;

  @JsonValue
  @Override
  public String toString() {
    return name().toLowerCase();
  }
}
