package de.schegge.fake.persona.feature;

import static de.schegge.fake.persona.PersonaAttribute.FIRSTNAME;
import static de.schegge.fake.persona.PersonaAttribute.LASTNAME;
import static de.schegge.fake.persona.PersonaAttribute.LOGIN;

import de.schegge.fake.persona.MapGate;
import de.schegge.fake.persona.PersonaAttribute;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoginByUsernameFeatureHandler extends FeatureHandler {

  private static final Logger logger = LoggerFactory.getLogger(LoginByUsernameFeatureHandler.class);

  public LoginByUsernameFeatureHandler() {
    super(PersonaAttribute.LOGIN);
  }

  @Override
  public void handle(Map<PersonaAttribute, List<String>> input, MapGate output,
      Map<PersonaAttribute, String> currentValues) {
    logger.debug("handle");
    String firstname = code(currentValues.get(FIRSTNAME));
    String lastname = code(currentValues.get(LASTNAME));
    String loginValue = firstname + "." + lastname;
    input.get(LOGIN).forEach(k -> output.putIfAllowed(k, loginValue));
  }
}
