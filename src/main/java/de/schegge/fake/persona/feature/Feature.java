package de.schegge.fake.persona.feature;

public enum Feature {
  OVERRIDE_EXITING_VALUES,
  OVERRIDE_ZERO_VALUES,
  NULL_ON_MISSING_ATTRIBUTE,
  RANDOM_VALUE,
  EMAIL_BY_USERNAME,
  LOGIN_BY_USERNAME,
}
