package de.schegge.fake.persona.feature;

import de.schegge.fake.persona.MapGate;
import de.schegge.fake.persona.PersonaAttribute;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public abstract class FeatureHandler {

  private final PersonaAttribute handledAttribute;

  public FeatureHandler(PersonaAttribute handledAttribute) {
    this.handledAttribute = handledAttribute;
  }

  public PersonaAttribute getHandledAttribute() {
    return handledAttribute;
  }

  public abstract void handle(Map<PersonaAttribute, List<String>> input, MapGate output,
      Map<PersonaAttribute, String> currentValues);

  protected String code(Object name) {
    return Objects.toString(name, "").replace(' ', '-').toLowerCase();
  }
}
