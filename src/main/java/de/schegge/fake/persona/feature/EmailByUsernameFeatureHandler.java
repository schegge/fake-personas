package de.schegge.fake.persona.feature;

import static de.schegge.fake.persona.PersonaAttribute.EMAIL;
import static de.schegge.fake.persona.PersonaAttribute.FIRSTNAME;
import static de.schegge.fake.persona.PersonaAttribute.LASTNAME;

import de.schegge.fake.persona.MapGate;
import de.schegge.fake.persona.PersonaAttribute;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EmailByUsernameFeatureHandler extends FeatureHandler {

  private static final Logger logger = LoggerFactory.getLogger(EmailByUsernameFeatureHandler.class);

  public EmailByUsernameFeatureHandler() {
    super(PersonaAttribute.EMAIL);
  }

  @Override
  public void handle(Map<PersonaAttribute, List<String>> input, MapGate output,
      Map<PersonaAttribute, String> currentValues) {
    logger.debug("handle");
    String email = currentValues.get(EMAIL);
    String firstname = code(currentValues.get(FIRSTNAME));
    String lastname = code(currentValues.get(LASTNAME));
    String emailValue = firstname + "." + lastname + email.substring(email.indexOf('@'));
    input.get(EMAIL).forEach(k -> output.putIfAllowed(k, emailValue));
  }
}
