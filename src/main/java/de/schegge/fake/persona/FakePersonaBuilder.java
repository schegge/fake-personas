package de.schegge.fake.persona;

import static de.schegge.fake.persona.PersonaAttribute.CITY;
import static de.schegge.fake.persona.PersonaAttribute.EMAIL;
import static de.schegge.fake.persona.PersonaAttribute.FIRSTNAME;
import static de.schegge.fake.persona.PersonaAttribute.GENDER;
import static de.schegge.fake.persona.PersonaAttribute.HOUSENUMBER;
import static de.schegge.fake.persona.PersonaAttribute.LASTNAME;
import static de.schegge.fake.persona.PersonaAttribute.LOGIN;
import static de.schegge.fake.persona.PersonaAttribute.NONE;
import static de.schegge.fake.persona.PersonaAttribute.PHONE;
import static de.schegge.fake.persona.PersonaAttribute.SALUTATION;
import static de.schegge.fake.persona.PersonaAttribute.STREET;
import static de.schegge.fake.persona.PersonaAttribute.TITLE;
import static de.schegge.fake.persona.PersonaAttribute.ZIP;
import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.schegge.fake.persona.feature.Feature;
import java.io.IOException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FakePersonaBuilder {

  private static final Logger logger = LoggerFactory.getLogger(FakePersonaBuilder.class);

  private static final Map<String, PersonaAttribute> DEFAULT_ALIASES = Map.ofEntries(
      Map.entry("gender", GENDER), Map.entry("sex", GENDER),
      Map.entry("title", TITLE),
      Map.entry("salutation", SALUTATION),
      Map.entry("firstname", FIRSTNAME), Map.entry("givenname", FIRSTNAME),
      Map.entry("lastname", LASTNAME), Map.entry("surname", LASTNAME),
      Map.entry("username", LOGIN), Map.entry("login", LOGIN),
      Map.entry("email", EMAIL), Map.entry("mail", EMAIL),
      Map.entry("telephone", PHONE), Map.entry("phone", PHONE),
      Map.entry("street", STREET),
      Map.entry("housenumber", HOUSENUMBER), Map.entry("number", HOUSENUMBER),
      Map.entry("zip", ZIP), Map.entry("city", CITY)
  );

  private final Set<PersonaAttribute> ignore;
  private final Set<PersonaAttribute> override;
  private final Map<String, Map<PersonaAttribute, List<String>>> personas;
  private final ObjectMapper objectMapper;
  private final Set<Feature> features;
  private final SecureRandom secureRandom;
  private final Map<String, PersonaAttribute> aliases;

  public static FakePersonaBuilder create() {
    return create("/personas.json");
  }

  public static FakePersonaBuilder create(String filename) {
    FakePersonaBuilder fakePersonaBuilder = new FakePersonaBuilder(new ObjectMapper());
    return fakePersonaBuilder.with(filename);
  }

  public FakePersonaBuilder(ObjectMapper objectMapper) {
    this.objectMapper = objectMapper;
    ignore = EnumSet.noneOf(PersonaAttribute.class);
    override = EnumSet.noneOf(PersonaAttribute.class);
    features = EnumSet.noneOf(Feature.class);
    secureRandom = new SecureRandom();
    aliases = new HashMap<>(DEFAULT_ALIASES);
    personas = new HashMap<>();
  }

  private FakePersonaBuilder(Set<PersonaAttribute> ignore, Set<PersonaAttribute> override,
      Map<String, Map<PersonaAttribute, List<String>>> personas, SecureRandom secureRandom, Set<Feature> features,
      ObjectMapper objectMapper, Map<String, PersonaAttribute> aliases) {
    this.ignore = EnumSet.copyOf(ignore);
    this.override = EnumSet.copyOf(override);
    this.features = EnumSet.copyOf(features);
    this.personas = personas;
    this.secureRandom = secureRandom;
    this.objectMapper = objectMapper;
    this.aliases = new HashMap<>(aliases);
  }

  public FakePersonaBuilder copy() {
    return new FakePersonaBuilder(ignore, override, personas, secureRandom, features, objectMapper, aliases);
  }

  public FakePersonaBuilder with(String filename) {
    logger.info("properties file={}", filename);
    if (Objects.requireNonNull(filename).isBlank()) {
      return this;
    }
    Map<String, Map<PersonaAttribute, List<String>>> additional;
    try {
      additional = objectMapper.readValue(getClass().getResourceAsStream(filename),
          new TypeReference<>() {
          });
    } catch (IOException e) {
      throw new ConversionException("cannot read personas: " + filename);
    }
    if (additional.isEmpty()) {
      throw new ConversionException("no personas defined: " + getClass().getResource(filename));
    }
    this.personas.putAll(additional);
    return this;
  }

  public FakePersonaBuilder alias(Map<String, PersonaAttribute> map) {
    aliases.putAll(map);
    return this;
  }

  public FakePersonaBuilder alias(String key, PersonaAttribute attribute) {
    aliases.put(key, attribute);
    return this;
  }

  public FakePersonaBuilder dealias(String key) {
    aliases.remove(key);
    return this;
  }

  public FakePersonaBuilder ignore(PersonaAttribute... attributes) {
    ignore.addAll(List.of(attributes));
    return this;
  }

  public FakePersonaBuilder override(PersonaAttribute... attributes) {
    override.addAll(List.of(attributes));
    return this;
  }

  public FakePersonaBuilder enable(Feature... features) {
    this.features.addAll(List.of(features));
    return this;
  }

  public FakePersonaBuilder disable(Feature... features) {
    this.features.removeAll(List.of(features));
    return this;
  }

  public <T> T build(String personaName, T bean) {
    requireNonNull(personaName);
    requireNonNull(bean);
    try {
      Map<String, Object> beanMap = objectMapper
          .readValue(objectMapper.writeValueAsString(bean), new TypeReference<>() {
          });
      logger.debug("personaName data: {}, {}", personaName, beanMap);
      Map<String, Object> filled = fillPersona(personaName, beanMap);
      logger.debug("personaName filled: {}, {}", personaName, filled);
      objectMapper.readerForUpdating(bean).readValue(objectMapper.writeValueAsString(filled));
    } catch (JsonProcessingException e) {
      throw new ConversionException("cannot fill with personaName " + personaName, e);
    }
    return bean;
  }

  public <T> T build(T t) {
    return build("", t);
  }

  private boolean ignoreUnknownOrIgnored(String e) {
    PersonaAttribute attribute = aliases.getOrDefault(e, NONE);
    return attribute != NONE && !ignore.contains(attribute);
  }

  private Map<String, Object> fillPersona(String persona, Map<String, Object> beanMap) {
    Map<PersonaAttribute, List<String>> personaAttributesMap = beanMap.keySet().stream()
        .filter(this::ignoreUnknownOrIgnored).collect(groupingBy(aliases::get));

    PersonaData personaData = getPersonaData(persona);
    List<String> usedKeys = aliases.entrySet().stream().filter(e -> override.contains(e.getValue())).map(Entry::getKey)
        .collect(toList());

    FeatureHandlers handlers = new FeatureHandlers(features, personaAttributesMap.keySet());
    MapGate output = new MapGate(beanMap, features, usedKeys);
    Map<PersonaAttribute, String> currentValues = new HashMap<>();
    for (Entry<PersonaAttribute, List<String>> entry : personaAttributesMap.entrySet()) {
      PersonaAttribute personaAttribute = entry.getKey();
      currentValues.put(personaAttribute, personaData.get(personaAttribute));
      if (handlers.isHandled(personaAttribute)) {
        continue;
      }

      String personaValue = currentValues.get(personaAttribute);
      for (String jsonKey : entry.getValue()) {
        output.putIfAllowed(jsonKey, personaValue);
      }
    }
    handlers.handle(personaAttributesMap, output, currentValues);
    return output.getMap();
  }

  private Map<PersonaAttribute, List<String>> getPersona(String persona) {
    if (!persona.isBlank()) {
      return Optional.ofNullable(personas.get(persona))
          .orElseThrow(() -> new ConversionException("unknown persona " + persona));
    }
    List<String> personaNames = new ArrayList<>(personas.keySet());
    return personas.get(personaNames.get(secureRandom.nextInt(personaNames.size())));
  }

  private PersonaData getPersonaData(String persona) {
    Map<PersonaAttribute, List<String>> data = getPersona(persona);
    return new PersonaData(data,
        features.contains(Feature.NULL_ON_MISSING_ATTRIBUTE),
        features.contains(Feature.RANDOM_VALUE), secureRandom);
  }
}
