package de.schegge.fake.persona;

import com.fasterxml.jackson.annotation.JsonValue;

public enum Gender {
  NONE,
  MALE,
  FEMALE,
  OTHER;

  @JsonValue
  @Override
  public String toString() {
    return name().toLowerCase();
  }
}
