package de.schegge.fake.persona;

import static de.schegge.fake.persona.PersonaAttribute.CITY;
import static de.schegge.fake.persona.PersonaAttribute.EMAIL;
import static de.schegge.fake.persona.PersonaAttribute.FIRSTNAME;
import static de.schegge.fake.persona.PersonaAttribute.LASTNAME;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import de.schegge.fake.persona.feature.Feature;
import de.schegge.fake.test.Alias;
import de.schegge.fake.test.Fake;
import de.schegge.fake.test.Id;
import de.schegge.fake.test.WithPersonas;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

@ExtendWith(PersonParameterResolver.class)
@WithPersonas
class FakePersonaTest {

  @Test
  void fakePersona(
      @Id(42) @Fake(persona = "mitarbeiterin", alias = @Alias(field = "nachname", attribute = LASTNAME)) PersonDto personDto) {
    assertEquals("Andrea", personDto.getFirstname());
    assertEquals("Mustermann", personDto.getLastname());
    assertEquals("andrea.mustermann@fakenews.de", personDto.getEmail());
    assertEquals(33334, personDto.getZip());
    assertEquals("Gütersloh", personDto.getCity());
    assertEquals(42L, personDto.getId());
  }

  @Test
  void fakePersonaWithIgnoredCity(PersonDto input) {
    PersonDto personDto = FakePersonaBuilder.create()
        .ignore(CITY)
        .build("mitarbeiterin", input);
    assertEquals("Andrea", personDto.getFirstname());
    assertEquals("Mustermann", personDto.getLastname());
    assertNull(personDto.getCity());
  }

  @Test
  void fakePersonaWithIgnoredEmail(@Fake(persona = "mitarbeiterin", ignore = EMAIL) PersonDto personDto) {
    assertEquals("Andrea", personDto.getFirstname());
    assertEquals("Mustermann", personDto.getLastname());
    assertNull(personDto.getEmail());
  }

  @Test
  void fakePersonaWithPreset(PersonDto input) {
    input.setFirstname("Arthur");
    input.setLastname("Dent");
    PersonDto output = FakePersonaBuilder.create()
        .enable(Feature.NULL_ON_MISSING_ATTRIBUTE).build(input);
    assertEquals("Arthur", output.getFirstname());
    assertEquals("Dent", output.getLastname());
  }

  @Test
  void fakePersonaWithPresetAndOverride(PersonDto input) {
    input.setFirstname("Arthur");
    input.setLastname("Dent");
    PersonDto output = FakePersonaBuilder.create()
        .enable(Feature.NULL_ON_MISSING_ATTRIBUTE)
        .enable(Feature.LOGIN_BY_USERNAME)
        .override(FIRSTNAME)
        .build("schurke", input);
    assertNotEquals("Arthur", output.getFirstname());
    assertEquals("Dent", output.getLastname());
  }

  @Test
  void fakeEmptyPersonas(PersonDto input) {
    assertThrows(ConversionException.class, () -> FakePersonaBuilder.create("/empty.json")
        .build("held", input));
  }

  @Test
  void fakeUnknownPersona(PersonDto input) {
    assertThrows(ConversionException.class, () -> FakePersonaBuilder.create()
        .build("held", input));
  }
}
