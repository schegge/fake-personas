package de.schegge.fake.persona;

public interface Person {

  String getFirstname();

  void setFirstname(String firstname);

  String getLastname();

  void setLastname(String lastname);

  String getEmail();

  void setEmail(String email);

  Long getId();

  void setId(Long id);

  Gender getGender();

  void setGender(Gender gender);
}
